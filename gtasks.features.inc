<?php
/**
 * @file
 * gtasks.features.inc
 */

/**
 * Implements hook_node_info().
 */
function gtasks_node_info() {
  $items = array(
    'google_list' => array(
      'name' => t('Google List'),
      'base' => 'node_content',
      'description' => t('Represent a Google list.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'google_task' => array(
      'name' => t('Google Task'),
      'base' => 'node_content',
      'description' => t('Represent a Google task'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
