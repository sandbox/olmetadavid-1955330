(function($) {
 
  $(document).ready(function() {
    
    // Load library on components.
    gtasks_list_refresh_components();
    
    // Add AJAX to create task form submit.
    $('#gtasks-create-task-form').submit(function() {
      
      // Get the form.
      var form = $(this);
      
      $.ajax({
        type: 'POST',
        url: '/js/gtasks/list/tasks/create',
        data: form.serialize(),
        success: function(data) {
          
          // Replace content list.
          $('#gtasks-list-container').html(data.content);
          
          // Load library on components.
          gtasks_list_refresh_components();
          
          // Clear the new task input.
          $('#gtasks-create-task-form input[name="name"]').val('');
        }
      })
      
      return false;
    });
  });
  
  function gtasks_list_refresh_components() {
    
    // Use sortable and editable only on current tasks.
    if (Drupal.settings.gtasks.list == 'current') {

      // Render the task list sortable.
      $('.gtasks-tasks-list').sortable({
        update: function(event, ui) {

          // Get the list_id.
          var list_id = $(this).attr('list_id');

          // Prepare string to send to server.
          var items = "";

          // Build the string to send.
          $(".gtasks-tasks-list").children().each(function (i) {
            var li = $(this);
            items += li.attr("nid") + ':' + i + ',';
          });

          // Send the string to server.
          $.ajax({
            type: 'POST',
            url: '/js/gtasks/list/tasks/reorder',
            data: {
              listId: list_id,
              data: items
            },
            dataType: 'json',
            success: function(data) {

              // Replace content list.
              $('#gtasks-list-container').html(data.content);

              // Load library on components.
              gtasks_list_refresh_components();
            }
          });
        }
      });

      // Render the tasks in place editable.
      $('.gtasks-tasks-list li').each(function() {

        // Store item.
        var item = $(this);

        // Get the task id.
        var nid = item.attr('nid');

        // Apply in place editing.
        item.find('label').editInPlace({
          url: '/js/gtasks/list/tasks/update/' + nid
        });
      });
    }
    
    // Apply callback on task checkbox.
    gtasks_apply_task_callback();
  }
  
  function gtasks_apply_task_callback() {
    $('.gtasks-tasks-list li').each(function() {
      
      // Store item.
      var item = $(this);
      
      // Get the task id.
      var nid = item.attr('nid');

      // Get the action type to realized.
      var action = item.attr('action');

      item.find('input[type="checkbox"]').click(function() {
        $.ajax({
          type: 'POST',
          url: '/js/gtasks/list/tasks/' + action + '/' + nid,
          data: {
            type: Drupal.settings.gtasks.list
          },
          dataType: 'json',
          success: function(data) {

            // Replace content list.
            $('#gtasks-list-container').html(data.content);

            // Load library on components.
            gtasks_list_refresh_components();
          }
        });
      });
    });
    
  }

})(jQuery);