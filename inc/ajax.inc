<?php

/**
 * @file
 * Contains AJAX callback.
 */

/**
 * Get tasks of the list to JS format.
 *
 * @param int $list_id
 *   The id of the list.
 */
function gtasks_js_tasks_refresh($list_id, $display_type = 'current') {

  // Get task list.
  if ($display_type == 'current') {
    $content = gtasks_get_current_tasks($list_id);
  }
  elseif ($display_type == 'history') {
    $content = gtasks_get_list_history($list_id);
  }

  // Return the content.
  drupal_json_output(array(
    'status' => 1,
    'content' => drupal_render($content)
  ));
  drupal_exit();
}

/**
 * Create new task.
 */
function gtasks_js_tasks_create() {

  // Get POST data.
  $list_id = check_plain($_POST['list_id']);
  $task_name = check_plain($_POST['name']);

  // Add the task.
  gtasks_add_task($list_id, $task_name);

  // Return the reloaded list.
  gtasks_js_tasks_refresh($list_id);
}

/**
 * Update existing task.
 *
 * @param int $nid
 *   The nid of the task to update.
 */
function gtasks_js_tasks_update($nid) {

  // Check nid.
  $nid = (int) $nid;

  // Get the new name.
  $task_name = check_plain($_POST['update_value']);

  // Update task.
  gtasks_update_task($nid, $task_name);

  // Return the name to display it.
  print $task_name;
  drupal_exit();
}

/**
 * Finish a task.
 *
 * @param int $nid
 *   The nid of the task to finish.
 */
function gtasks_js_tasks_finish($nid) {

  // Check nid.
  $nid = (int) $nid;

  // Get the display type.
  $type = check_plain($_POST['type']);
  
  // Finish the task.
  $task = gtasks_finish_task($nid);

  // Refresh the task list.
  gtasks_js_tasks_refresh($task->field_google_list[LANGUAGE_NONE][0]['target_id'], $type);
}

/**
 * Open a task.
 *
 * @param int $nid
 *   The nid of the task to open.
 */
function gtasks_js_tasks_open($nid) {

  // Check nid.
  $nid = (int) $nid;

  // Get the display type.
  $type = check_plain($_POST['type']);
  
  // Finish the task.
  $task = gtasks_open_task($nid);

  // Refresh the task list.
  gtasks_js_tasks_refresh($task->field_google_list[LANGUAGE_NONE][0]['target_id'], $type);
}

/**
 * Reorder tasks.
 */
function gtasks_js_tasks_reorder() {

  // Get the list id.
  $list_id = $_POST['listId'];

  // Get new list order.
  $str_items = $_POST['data'];

  // Explode on ',' character.
  $items = explode(',', $str_items);

  // Reorder each task.
  foreach ($items as $item) {
    $parts = explode(':', $item);

    if (count($parts) != 2) {
      continue;
    }

    // Change order.
    gtasks_reorder_task($parts[0], $parts[1]);
  }

  gtasks_js_tasks_refresh($list_id);
}
