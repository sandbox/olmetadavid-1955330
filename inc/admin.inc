<?php

/**
 * @file
 * Contains admin functions.
 */

/**
 * GTasks admin form.
 */
function gtasks_admin_form() {
  
  $form = array();
  
  $form['gtasks_activate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate the Google connection for tasks synchronization.'),
    '#default_value' => variable_get('gtasks_activate', 0),
  );
  
  return system_settings_form($form);
}